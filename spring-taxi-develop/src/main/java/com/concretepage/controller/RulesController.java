package com.concretepage.controller;
import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;

import com.concretepage.entity.Article;
import com.concretepage.entity.Greeting;
import com.concretepage.entity.Login_Validation;
import com.concretepage.entity.RulesProperties;
import com.concretepage.entity.CabSearchList;
import com.concretepage.service.IArticleService;

@Controller
@RequestMapping(value="/properties")
@CrossOrigin(origins = {"*"},allowCredentials = "false")
public class RulesController {
	
	@Autowired
	private IArticleService articleService;	
	//@PostMapping("/newrule")
	@RequestMapping(value = "/newrule", method = RequestMethod.POST)
	public ResponseEntity<Void> createRule(@RequestBody RulesProperties rulesProperties, UriComponentsBuilder builder) {
		
		boolean flag = articleService.createRules(rulesProperties);
        if (flag == false) {
        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/newrule?id={id}").buildAndExpand(rulesProperties.getBranch_id()).toUri());
        //System.out.println("WELCOME TEST 2"+article.getArticleId());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}	
	
	@RequestMapping("getRules")
	public ResponseEntity<List<RulesProperties>> getRules() {
		System.out.println("Got user list");
		List<RulesProperties> list = articleService.getRuleList();
		return new ResponseEntity<List<RulesProperties>>(list, HttpStatus.OK);
	}
	
	
} 