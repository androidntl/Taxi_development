package com.concretepage.controller;
import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;

import com.concretepage.entity.Article;
import com.concretepage.entity.Greeting;
import com.concretepage.entity.Login_Validation;
import com.concretepage.entity.RulesProperties;
import com.concretepage.service.IArticleService;

@Controller
@RequestMapping(value="/user")
@CrossOrigin(origins = {"http://localhost:4200"},allowCredentials = "false")
//@CrossOrigin(origins = {"http://localhost:2222"},allowCredentials = "false")
public class ArticleController {
	
	@Autowired
	private IArticleService articleService;
	@GetMapping("article")
	
	//mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
	
	public ResponseEntity<Article> getArticleById(@RequestParam("id") String id) {
		Article article = articleService.getArticleById(Integer.parseInt(id));
		return new ResponseEntity<Article>(article, HttpStatus.OK);
	}
	
	/*	@PostMapping("login")
public ResponseEntity<Void> handle() {
		System.out.println("HI test");
		   HttpHeaders responseHeaders = new HttpHeaders();
		   responseHeaders.set("MyResponseHeader", "MyValue");
		   return new ResponseEntity<Void>(responseHeaders, HttpStatus.CREATED);
		 }*/
	/*public ResponseEntity<List<Article>> greetingSubmit(@RequestParam("email_id") String email ,@RequestParam("pwd") String pass) {
		System.out.println("testprint "+email);
		System.out.println("testprint "+pass);
		List<Article> list =  articleService.login(email,pass);
		return new ResponseEntity<List<Article>>(list, HttpStatus.OK);
      // return new ResponseEntity<String>("got result"+email,HttpStatus.CREATED);
		
	}	*/	
	
	@RequestMapping(value = "/login_validations", method = RequestMethod.POST)
	public ResponseEntity<Void> login(@RequestBody Login_Validation obj, UriComponentsBuilder builder) {
		System.out.println("Email : "+obj.getEmail());
		System.out.println("password : "+obj.getPassword());
		boolean flag = articleService.Login_Validation(obj);
		//System.out.println("Flag result"+flag);
		//List<Article> list =  articleService.login( greeting );
		System.out.println(HttpStatus.OK);
		return new ResponseEntity<Void>( HttpStatus.OK);
      // return new ResponseEntity<String>("got result"+email,HttpStatus.CREATED);
		
	}	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<Void> login(@RequestBody Greeting greeting, UriComponentsBuilder builder) {
		System.out.println("Email : "+greeting.getEmail());
		System.out.println("password : "+greeting.getPassword());
		boolean flag = articleService.login(greeting);
		System.out.println("Flag result"+flag);
		//List<Article> list =  articleService.login( greeting );
		return new ResponseEntity<Void>( HttpStatus.OK);
      // return new ResponseEntity<String>("got result"+email,HttpStatus.CREATED);
		
	}	
	
	@RequestMapping(value = "/newrule", method = RequestMethod.POST)
	public ResponseEntity<Void> createRule(@RequestBody RulesProperties rulesProperties, UriComponentsBuilder builder) {
		System.out.println("branch_id : "+rulesProperties.getBranch_id());
		System.out.println("properties_name : "+rulesProperties.getProperties_name());
		boolean flag = articleService.createRules(rulesProperties);
        if (flag == false) {
        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/newrule?id={id}").buildAndExpand(rulesProperties.getBranch_id()).toUri());
        //System.out.println("WELCOME TEST 2"+article.getArticleId());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}	
	  
	/*public ResponseEntity<Greeting> (@RequestBody Greeting greeting) {
		System.out.println("formsubmited");
		
		
	}*/
	/*public String greetingSubmit(@ModelAttribute Greeting greeting) {
		System.out.println("Get TEST");
        return "result";
    }*/
	/*public String greetingSubmit(@RequestParam("email") String email ) {
		System.out.println("testprint "+email);
        return "Test"+ email;
    }*/
	@GetMapping("all-articles")
	public ResponseEntity<List<Article>> getAllArticles() {
		List<Article> list = articleService.getAllArticles();
		return new ResponseEntity<List<Article>>(list, HttpStatus.OK);
	}
	@RequestMapping("getAllUsers")
	public ResponseEntity<List<Greeting>> getAllUsers() {
		System.out.println("Got user list");
		List<Greeting> list = articleService.getAllUsers();
		return new ResponseEntity<List<Greeting>>(list, HttpStatus.OK);
	}
	@PostMapping("/article")
	public ResponseEntity<Void> createArticle(@RequestBody Article article, UriComponentsBuilder builder) {
		boolean flag = articleService.createArticle(article);
        if (flag == false) {
        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/article?id={id}").buildAndExpand(article.getArticleId()).toUri());
        //System.out.println("WELCOME TEST 2"+article.getArticleId());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	@PutMapping("article")
	public ResponseEntity<Article> updateArticle(@RequestBody Article article) {
		articleService.updateArticle(article);
		return new ResponseEntity<Article>(article, HttpStatus.OK);
	}
	@DeleteMapping("article")
	public ResponseEntity<Void> deleteArticle(@RequestParam("id") String id) {
		articleService.deleteArticle(Integer.parseInt(id));
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}	
} 