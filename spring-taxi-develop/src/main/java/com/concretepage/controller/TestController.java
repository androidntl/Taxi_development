package com.concretepage.controller;

import com.concretepage.domain.CabDetail;
import com.concretepage.domain.Message;
import com.concretepage.domain.Tariff;
import com.concretepage.enums.EnumMessage;
import java.util.ArrayList;

import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
@RequestMapping("/")
@CrossOrigin(origins = {"http://localhost:4200"},allowCredentials = "false")
public class TestController {

    @Autowired
    private KieSession kSession;

    @RequestMapping("/index")
    @ResponseBody
    String index() {
        return "index Page!";
    }

    @RequestMapping(value = "/droolsTest",method = RequestMethod.GET)
    @ResponseBody
    public String droolsTest() {
        try {
        	
        	CabDetail cabDetail=new CabDetail();
        	 ArrayList<CabDetail> cabList=new ArrayList<CabDetail>();  
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","mini","12.998966, 80.202160",1,3.5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","micro","12.998866, 80.202260",2,4.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","suv","12.988966, 80.212160",6,1.5));
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","sedan","12.997966, 80.202260",6,5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","mini","12.998866, 80.203160",2,4.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","micro","12.999966, 80.202190",5,3.0));
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","suv","12.978966, 80.202170",3,4.5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","sedan","12.998266, 80.202560",4,3.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","mini","12.998566, 80.203160",5,2.5));
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","micro","12.998966, 80.204160",5,2.5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","suv","12.993966, 80.202160",2,1.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","sedan","12.998966, 80.222160",1,3.5));
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","mini","12.998966, 80.242160",1,4.5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","micro","12.996966, 80.202160",2,3.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","suv","12.998966, 80.203060",3,2.5));
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","sedan","12.998966, 80.202160",6,4.5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","mini","12.998966, 80.206160",4,2.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","micro","12.998966, 80.232160",4,3.5));
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","micro","12.998966, 80.2282160",36,2.5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","suv","12.998966, 80.202160",12,3.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","suv","12.958966, 80.202160",10,4.5));
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","sedan","12.968966, 80.202160",2,5.5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","mini","12.992966, 80.202160",1,3.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","mini","12.994966, 80.202160",5,4.5));
             cabList.add(new CabDetail(4, 1, 1, 100, "1234", "Sedan", "12.123,80.123", "2017-09-13 16:34:00","sedab","12.996966, 80.203160",4,3.5));  
             cabList.add(new CabDetail(2, 1, 1, 200, "1234", "Hachback", "12.123,80.123", "2017-09-13 16:34:00","sedan","12.998966, 80.208860",2,4.5));   
             cabList.add(new CabDetail(4, 1, 1, 3100, "1234", "SUV", "12.123,80.123", "2017-09-13 16:34:00","micro","12.998966, 80.207760",3,3.5));
             
             Tariff tariff = new Tariff();
             ArrayList<Tariff> tariffList=new ArrayList<Tariff>(); 
             tariffList.add(new Tariff("micro","100"));
             tariffList.add(new Tariff("mini","150"));
             tariffList.add(new Tariff("sedan","200"));
             tariffList.add(new Tariff("suv","250"));
             
             for(CabDetail b:cabList){  
            	 if(b.getFltDistance()<=4 & b.getStrCabType().equals("mini")) {
            		// System.out.println("Execution rule ends"+b.getStrCabType() );
            		 kSession.insert(b);// insert
            		 for(Tariff t:tariffList){ 
            			 kSession.insert(t);
            			 kSession.fireAllRules();// Enforce the rules
            		 }                    
            	 }               	
                // kSession.dispose();  
             }
          /*  Message message = new Message();
            message.setMessage("good bye drools");
            message.setStatus(EnumMessage.BYE.getCode());           
            System.out.println("Start executing rules" + message);
            kSession.insert(message);// insert
            //kSession.getAgenda().getAgendaGroup( "GoodBye" ).setFocus();
            kSession.fireAllRules();// Enforce the rules
            kSession.dispose();
            System.out.println("Execution rule ends" );*/
            return cabDetail.getStrCabId();
        } catch (Throwable t) {
            t.printStackTrace();
            return "error";
        }
    }

}
