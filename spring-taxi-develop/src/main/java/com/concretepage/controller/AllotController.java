package com.concretepage.controller;

import com.concretepage.dao.ShuttleRouteStopsDao;
import com.concretepage.domain.CabDetail;
import com.concretepage.domain.CabSearch;
import com.concretepage.domain.CabSearchResponse;
import com.concretepage.domain.Tariff;
import com.concretepage.entity.Booking;
import com.concretepage.entity.CabBooking;
import com.concretepage.entity.CabBookingRule;
import com.concretepage.entity.CabSearchList;
import com.concretepage.entity.CustomerDetail;
import com.concretepage.entity.Location;
import com.concretepage.entity.RulesProperties;
import com.concretepage.entity.TariffDetail;
import com.concretepage.model.RouteStopsResponse;
import com.concretepage.service.IArticleService;
import com.concretepage.service.ReloadDroolsRulesService;
import com.concretepage.utils.GeoDistanceUtil;
import com.google.gson.Gson;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;



@Controller
@RequestMapping("/cab")
@CrossOrigin(origins = {"*"},allowCredentials = "false")
public class AllotController {

	@Resource
    private ReloadDroolsRulesService rules;
	
    @Autowired
    private KieSession kSession;
    
    @Autowired
	private IArticleService articleService;
    
    @Autowired
    private ShuttleRouteStopsDao stopsRepo;  
  

    @RequestMapping("/index")
    @ResponseBody
    String index() {
        return "index Page!";
    }
    
    @RequestMapping(value = "/allot", method = RequestMethod.POST)
	public ResponseEntity<Void> cabAllot(@RequestBody CabSearchList cabSearchList, UriComponentsBuilder builder) {	
		System.out.println("CAB MODEL : "+ cabSearchList.getCab_model());		
		boolean flag = articleService.allotCab(cabSearchList);
        if (flag == false) {
        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/newrule?id={id}").buildAndExpand(cabSearchList.getCab_type()).toUri());
        //System.out.println("WELCOME TEST 2"+article.getArticleId());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}	
    
    @RequestMapping(value ="/getCab",method = RequestMethod.GET)
	public ResponseEntity<List<CabSearchList>> getCab(@RequestBody CabSearchList cabSearchList, UriComponentsBuilder builder) {
		System.out.println("Got cab list");
		List<CabSearchList> list = articleService.getCabList();
		List<RulesProperties> ruleList=articleService.getRuleList();		
		try {		
		  for(CabSearchList csl:list){ 
			  csl.setDistance(new GeoDistanceUtil().distance(csl.getCab_lat(),csl.getCab_lng(),cabSearchList.getCab_lat(),cabSearchList.getCab_lng(),"K"));
			  System.out.println("Distance : "+csl.getDistance());
			  for(RulesProperties rl:ruleList){ 
         		 kSession.insert(csl);
         		 kSession.insert(rl);// insert         		 
			  }			 
          }		
		  kSession.fireAllRules(); 
		  kSession.destroy();
		  } catch (Throwable t) {
	            t.printStackTrace();	           
	        }
		return new ResponseEntity<List<CabSearchList>>(list, HttpStatus.OK);
	}

    @ResponseBody
    @RequestMapping(value ="/caballot",method = RequestMethod.POST)
    public ResponseEntity<List<CabSearchList>> cabAllotDynamic(@RequestBody CabSearchList cabSearchList, UriComponentsBuilder builder) {
    	rules.reload("allot_new");
    	KieSession kieSession = ReloadDroolsRulesService.kieContainer.newKieSession();
        List<CabSearchList> list = articleService.getCabList();
		List<RulesProperties> ruleList=articleService.getRuleList();
		CabBooking cabBooking= new CabBooking();
		CabBookingRule bookingRule = new CabBookingRule();
		bookingRule.setRuleList(ruleList);
		bookingRule.setBookingType("cab_allot");
		
		try {	
			for(CabSearchList csl:list){ 
				  csl.setDistance(new GeoDistanceUtil().distance(csl.getCab_lat(),csl.getCab_lng(),cabSearchList.getCab_lat(),cabSearchList.getCab_lng(),"K"));
			 }
			 list = articleService.getCabList();
			  for(CabSearchList csl:list){  
				 // csl.setDistance(new GeoDistanceUtil().distance(csl.getCab_lat(),csl.getCab_lng(),cabSearchList.getCab_lat(),cabSearchList.getCab_lng(),"K"));
				  System.out.println("Distance : "+csl.getDistance());
				  for(RulesProperties rl:ruleList){ 
					  kieSession.insert(csl);
					  kieSession.insert(rl);// insert
					  kieSession.fireAllRules(); 
				  }
	          }	
			  kieSession.destroy();
			  } catch (Throwable t) {
		            t.printStackTrace();		           
		        }    
		/*	try {	
			 for(CabSearchList csl:list){ 
				  csl.setDistance(new GeoDistanceUtil().distance(csl.getCab_lat(),csl.getCab_lng(),cabSearchList.getCab_lat(),cabSearchList.getCab_lng(),"K"));
			 }
			 list = articleService.getCabList();
			 cabBooking.setCabList(list);
			 cabBooking.setCabModel("mini");
			 cabBooking.setBookingNo("123456");
			 kSession.insert(cabBooking);
     		 kSession.insert(bookingRule);// insert
			  for(CabSearchList csl:list){ 
				 // csl.setDistance(new GeoDistanceUtil().distance(csl.getCab_lat(),csl.getCab_lng(),cabSearchList.getCab_lat(),cabSearchList.getCab_lng(),"K"));
				  System.out.println("Distance : "+csl.getDistance());
				  for(RulesProperties rl:ruleList){ 
	         		 kSession.insert(csl);
	         		 kSession.insert(rl);// insert
	         		 kSession.fireAllRules(); 
				  }
	          }			 
			 // kSession.destroy();     		
    		//System.out.println("Distance : "+bookingRule.getBookingType());			 
			  } catch (Throwable t) {
		            t.printStackTrace();	           
		        }*/	
		return new ResponseEntity<List<CabSearchList>>(list, HttpStatus.OK);
    }
    
    @ResponseBody
    @RequestMapping(method = RequestMethod.GET,value="/route/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    //@RequestMapping("/carlist.json")
    public String  getshuttle(@PathVariable("id") Integer stop_id,HttpServletResponse response) {   
        response.setHeader("Access-Control-Allow-Origin", "*");//* or origin as u prefer
        response.setHeader("Access-Control-Allow-Credentials", "true");
      //  return new ResponseEntity(stopsRepo.getRouteStops(stop_id), HttpStatus.OK);
         return stopsRepo.getRouteStops(stop_id);  
    } 
   
   
    
    @ResponseBody
    @RequestMapping("/cabsearch/{cabType}/pick/{picklatLng}/drop/{droplatLng}")
    public String cabSearch(@PathVariable String cabType,@PathVariable String picklatLng,@PathVariable String droplatLng) {    	
   	    double trfPerKM=0;
   	    String[] values = picklatLng.split(",");
 	    String[] dvalues = droplatLng.split(",");   
 	    ArrayList<CabSearchResponse> cabList=new ArrayList<CabSearchResponse>();  	   
    	List<CabSearchList> list = articleService.getCabByDist(cabType,Double.parseDouble(values[0]),Double.parseDouble(values[1]));
    	TariffDetail trf = articleService.tariffById(cabType);
    	trfPerKM= trf.getTariff_per_km();		
    	for(CabSearchList csl:list){ 
    		double travelDistance =   new GeoDistanceUtil().distance(csl.getCab_lat(),csl.getCab_lng(),Double.parseDouble(values[0]),Double.parseDouble(values[1]),"K");
			cabList.add(new CabSearchResponse(csl, (int)(trfPerKM*travelDistance),travelDistance))  ;
		 }
   		String json = new Gson().toJson(cabList);   		
		return json;
	}
    
    
	@ResponseBody
	@RequestMapping(value = "/cabrequest/{cabType}/pick/{picklatLng}/drop/{droplatLng}/customerid/{customerId}")
	public String cabRequest(@PathVariable String cabType, @PathVariable String picklatLng,
			@PathVariable String droplatLng, @PathVariable int customerId) {
		rules.reload("cab_request");
		KieSession kieSession = ReloadDroolsRulesService.kieContainer.newKieSession();
		String[] values = picklatLng.split(",");
		String[] dvalues = droplatLng.split(",");
		List<CabSearchList> list = articleService.getCabByDist(cabType, Double.parseDouble(values[0]),
				Double.parseDouble(values[1]));
		List<RulesProperties> ruleList = articleService.getRuleList();
		CustomerDetail customerDetail = articleService.getCustById(customerId);
		try {
			for (CabSearchList csl : list) {
				for (RulesProperties rl : ruleList) {
					kieSession.insert(csl);
					kieSession.insert(rl);
					kieSession.insert(customerDetail);// insert
					kieSession.fireAllRules();
				}
			}
			kieSession.destroy();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		String json = new Gson().toJson(list);
		return json;
	}
	
	@ResponseBody
    @RequestMapping(value ="/tripClose/bookingid/{bookingId}/cabid/{cabId}")
    public void cabAllotDynamic(@PathVariable int bookingId,@PathVariable int cabId) {   
		rules.reload("trip_close");
		KieSession kieSession = ReloadDroolsRulesService.kieContainer.newKieSession();
		Booking booking = articleService.getBookingById(bookingId);
		CustomerDetail customerDetail =articleService.getCustById(booking.getCustomer_id());
		CabSearchList cabSearchList = articleService.getCabById(cabId);
		RulesProperties rulesProperties = articleService.getRuleByName(cabSearchList.getCab_model());
		TariffDetail tariffDetail = articleService.tariffById(booking.getTariff_id());
		try {			
			kSession.insert(booking);
			kSession.insert(cabSearchList);
			kSession.insert(customerDetail);	
			kSession.insert(rulesProperties);
			kSession.insert(tariffDetail);	
			kSession.fireAllRules();	 			
			kSession.destroy();
		} catch (Throwable t) {
			t.printStackTrace();
		}
    }
    
    @ResponseBody
    @RequestMapping("/location")
    public String retrieveLocation() {   
    	List<Location> locList =  articleService.getLocByBranch();     	
    	String json = new Gson().toJson(locList);   		
		return json;
    }
    
   
    @ResponseBody
    @RequestMapping("/cabtype/{cabType}/pick/{picklatLng}/drop/{droplatLng}")
    public String retrieveDetailsForCourse(@PathVariable String cabType,@PathVariable String picklatLng,@PathVariable String droplatLng) {    	
   	    double trfPerKM=0;
   	    String[] values = picklatLng.split(",");
 	    String[] dvalues = droplatLng.split(",");
    	ArrayList<CabSearchResponse> cabList=new ArrayList<CabSearchResponse>(); 
    	List<TariffDetail> tariffDetail =  articleService.getTariffById(cabType); 
    	List<CabSearchList> list = articleService.getCabByType(cabType);    	
    	for(CabSearchList csl:list){ 
			  csl.setDistance(new GeoDistanceUtil().distance(csl.getCab_lat(),csl.getCab_lng(),Double.parseDouble(values[0]),Double.parseDouble(values[1]),"K"));
		 }
    	for(TariffDetail trf:tariffDetail){ 
    		trfPerKM= trf.getTariff_per_km();
		 }
    	double travelDistance=new GeoDistanceUtil().distance(Double.parseDouble(values[0]),Double.parseDouble(values[1]),Double.parseDouble(dvalues[0]),Double.parseDouble(dvalues[1]),"K");
    	//list = articleService.getCabByType(cabType);
   		System.out.println("Distance : "+travelDistance +" : "+trfPerKM);
   		//cabList.add(new CabSearchResponse(cabType,(int)(trfPerKM*travelDistance),list));
   		//System.out.println("Distance : "+cabList);   
   		String json = new Gson().toJson(list);   		
		return json;
	}
    
    @ResponseBody
    @RequestMapping("/cabdist/{cabType}/pick/{picklatLng}/drop/{droplatLng}")
    public String retrieveCabDist(@PathVariable String cabType,@PathVariable String picklatLng,@PathVariable String droplatLng) {    	
   	    double trfPerKM=0;
   	    String[] values = picklatLng.split(",");
 	    String[] dvalues = droplatLng.split(",");   
 	    ArrayList<CabSearchResponse> cabList=new ArrayList<CabSearchResponse>(); 
 	    List<TariffDetail> tariffDetail =  articleService.getTariffById(cabType); 
    	List<CabSearchList> list = articleService.getCabByDist(cabType,Double.parseDouble(values[0]),Double.parseDouble(values[1])); 
    	for(TariffDetail trf:tariffDetail){    		
    		trfPerKM= trf.getTariff_per_km();
		 }
    	for(CabSearchList csl:list){ 
    		double travelDistance =   new GeoDistanceUtil().distance(csl.getCab_lat(),csl.getCab_lng(),Double.parseDouble(values[0]),Double.parseDouble(values[1]),"K");
			cabList.add(new CabSearchResponse(csl, (int)(trfPerKM*travelDistance),travelDistance))  ;
		 }
   		String json = new Gson().toJson(cabList);   		
		return json;
	}
    
    
    @ResponseBody
    @RequestMapping("/reload")
    public String reload() throws IOException {
        rules.reload("cab_allot");        
        return "ok";
    }
  
}
