package com.concretepage.enums;

/**
 *
 * @author lichangchao
 * @version 1.0.0
 * @date 2017/8/16 9:33
 * @see
 */
public enum EnumMessage {
    HELLO(1, "hello drools"), BYE(2, "good bye drools"),CABMATCH(4,"SUV"),CABSEARCH("SUV",6,5,8);
    private int    code;
    private String value;
    private int dist,rate,tariff;
    

    EnumMessage(int code, String value) {
        this.code = code;
        this.value = value;
    }
    
    EnumMessage(String value,int distance,int rate,int tariff) {
        this.dist = distance;
        this.value = value;
        this.rate=rate;
        this.tariff=tariff;
    }

    public int getDist() {
		return dist;
	}

	public void setDist(int dist) {
		this.dist = dist;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getTariff() {
		return tariff;
	}

	public void setTariff(int tariff) {
		this.tariff = tariff;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
