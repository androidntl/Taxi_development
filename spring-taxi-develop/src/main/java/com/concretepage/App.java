package com.concretepage;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.concretepage.config.DroolsAutoConfiguration;


@Configuration
@SpringBootApplication
@Import(DroolsAutoConfiguration.class)
public class App {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(App.class, args);        
    } 
    
}
