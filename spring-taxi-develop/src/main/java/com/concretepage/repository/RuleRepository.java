package com.concretepage.repository;

import com.concretepage.domain.Rule;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RuleRepository extends JpaRepository<Rule, Long> {
    List<Rule> findByRuleKey(String ruleKey);
}