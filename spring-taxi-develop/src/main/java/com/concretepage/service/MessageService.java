package com.concretepage.service;

import lombok.NonNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concretepage.entity.Booking;
import com.concretepage.entity.CabSearchList;
import com.concretepage.entity.RulesProperties;

/**
 *
 * @author lichangchao
 * @version 1.0.0
 * @date 2017/8/16 9:42
 * @see
 */
@Service
public class MessageService {
	@Autowired
	private IArticleService articleService;
	
  public void outMsg(@NonNull int name,@NonNull int custId){
    //System.out.println("MessageService:  "+name);
	  CabSearchList cabSearchList = articleService.getCabById(name);
	  articleService.cabAllot(cabSearchList) ;
	  Booking booking = new Booking(custId,name);
	  boolean flag = articleService.createBooking(booking) ;
	  System.out.println("CUSTOMER_id:  "+custId +" : "+flag);
  }
  
  public  void closeTrip(@NonNull int bookingId,@NonNull int dist,@NonNull int tariffPerKm,@NonNull int custType,@NonNull int propVal){
	   // System.out.println("MessageService:  "+bookingId +":"+ dist +":" + tariffPerKm +" :" +custType+":"+propVal);
		  RulesProperties rulesProperties = articleService.getRuleByName(""+custType);
		  int totalAmt =   tariffPerKm * dist ;
		  System.out.println(""+Integer.parseInt(rulesProperties.getProperties_value())+":"+totalAmt);
		  double custAmt =(totalAmt*(100-Integer.parseInt(rulesProperties.getProperties_value())))/100;
		  double driverAmt =  (totalAmt *propVal)/100;
		  double corpAmt =totalAmt-driverAmt;
		  System.out.println("CUSTOMER_id:"+(int)custAmt+":"+(int)driverAmt+":"+(int)corpAmt);
		  Booking booking = new Booking(bookingId,dist,totalAmt,(int)custAmt,(int)driverAmt,(int)corpAmt);
		  articleService.updateBooking(booking);
		  System.out.println("CUSTOMER_id:");
	  }
}
