package com.concretepage.service;

import java.util.List;

import com.concretepage.entity.Article;
import com.concretepage.entity.Booking;
import com.concretepage.entity.Greeting;
import com.concretepage.entity.Location;
import com.concretepage.entity.Login_Validation;
import com.concretepage.entity.RulesProperties;
import com.concretepage.entity.TariffDetail;
import com.concretepage.entity.CabSearchList;
import com.concretepage.entity.CustomerDetail;

public interface IArticleService {
     List<Article> getAllArticles();
     Article getArticleById(int articleId);
     boolean createArticle(Article article);
     void updateArticle(Article article);
     void deleteArticle(int articleId);
     boolean login(Greeting greeting);
     List<Greeting> getAllUsers();
     boolean Login_Validation(Login_Validation logv);
     boolean createRules(RulesProperties rulesProperties);
     RulesProperties getRuleById(int ruleId);
     boolean allotCab(CabSearchList cabSearchList);
     List<CabSearchList> getCabList();
     List<RulesProperties> getRuleList();
     RulesProperties getRuleByName(String ruleName);
     boolean cabAllot(CabSearchList cabSearchList);
     CabSearchList getCabById(int cabId);
     List<TariffDetail> getTariffById(String tariffType);
     TariffDetail tariffById(int tariffId);     
     TariffDetail tariffById(String tariffType);
     List<CabSearchList> getCabByType(String cabType);
     List<CabSearchList> getCabByDist(String cabType,double picLat,double picLng);
     List<Location> getLocByBranch();
     CustomerDetail getCustById(int customerId);
     boolean createBooking(Booking booking);
     void updateBooking(Booking booking);
     Booking getBookingById(int bookingId);
     
}
