package com.concretepage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concretepage.entity.Booking;
import com.concretepage.entity.CabSearchList;
import com.concretepage.entity.RulesProperties;
import com.concretepage.entity.TariffDetail;

import lombok.NonNull;

@Service
public class TripClose {
	@Autowired
	private IArticleService articleService;
	
  public  void closeTrip(@NonNull int bookingId,@NonNull int dist,@NonNull int tariffPerKm,@NonNull int custType,@NonNull int propVal){
   // System.out.println("MessageService:  "+bookingId +":"+ dist +":" + tariffPerKm +" :" +custType+":"+propVal);
	  RulesProperties rulesProperties = articleService.getRuleByName(""+custType);
	  int totalAmt =   tariffPerKm * propVal ;
	  double custAmt = totalAmt * ((100-Integer.parseInt(rulesProperties.getProperties_value()))/100);
	  double driverAmt = totalAmt * ((100-Integer.parseInt(rulesProperties.getProperties_value()))/100);
	  double corpAmt =totalAmt-(custAmt-driverAmt);
	  Booking booking = new Booking(bookingId,dist,totalAmt,(int)custAmt,(int)driverAmt,(int)driverAmt);
	  boolean flag = articleService.createBooking(booking) ;
	  System.out.println("CUSTOMER_id:"+flag);
  }
}
