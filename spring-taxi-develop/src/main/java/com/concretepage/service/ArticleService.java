package com.concretepage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concretepage.dao.IArticleDAO;
import com.concretepage.entity.Article;
import com.concretepage.entity.Booking;
import com.concretepage.entity.Greeting;
import com.concretepage.entity.Location;
import com.concretepage.entity.Login_Validation;
import com.concretepage.entity.RulesProperties;
import com.concretepage.entity.TariffDetail;
import com.concretepage.entity.CabSearchList;
import com.concretepage.entity.CustomerDetail;

@Service
public class ArticleService implements IArticleService {
	@Autowired
	private IArticleDAO articleDAO;
	@Override
	public Article getArticleById(int articleId) {
		Article obj = articleDAO.getArticleById(articleId);
		return obj;
	}	
	@Override
	public List<Article> getAllArticles(){
		return articleDAO.getAllArticles();
	}
	@Override
	public synchronized boolean createArticle(Article article){
       if (articleDAO.articleExists(article.getTitle(), article.getCategory())) {
    	   return false;
       } else {
    	   articleDAO.createArticle(article);
    	   return true;
       }
	}
	@Override
	public void updateArticle(Article article) {
		articleDAO.updateArticle(article);
	}
	@Override
	public void deleteArticle(int articleId) {
		articleDAO.deleteArticle(articleId);
	}
	@Override
	public boolean login(Greeting greeting) {
		// TODO Auto-generated method stub
		System.out.println("services called"+greeting);
		 articleDAO.login(greeting);
		 return true;
	}
	@Override
	public List<Greeting> getAllUsers() {
		// TODO Auto-generated method stub
		return articleDAO.getAllUsers();
	}
	@Override
	public boolean Login_Validation(Login_Validation logv) {
		// TODO Auto-generated method stub
	return	articleDAO.Login_Validation(logv);
		//return false;
	}
	@Override
	public boolean createRules(RulesProperties rulesProperties) {
		// TODO Auto-generated method stub
		 if (articleDAO.ruleExist(rulesProperties.getProperties_name())) {
	    	   return false;
	       } else {
	    	   articleDAO.createRule(rulesProperties);
	    	   return true;
	       }		
	}
	@Override
	public RulesProperties getRuleById(int ruleId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean allotCab(CabSearchList cabSearchList) {
		// TODO Auto-generated method stub	 
		
	    	   return true;	      		
	}
	
	@Override
	public List<CabSearchList> getCabList() {
		// TODO Auto-generated method stub
		return articleDAO.getCabList();
	}
	@Override
	public boolean cabAllot(CabSearchList cabSearchList) {
		// TODO Auto-generated method stub
		articleDAO.cabAllot(cabSearchList);
		return false;
	}
	@Override
	public CabSearchList getCabById(int cabId) {
		// TODO Auto-generated method stub
		CabSearchList obj = articleDAO.getCabById(cabId);
		return obj;
		//return null;
	}
	@Override
	public List<RulesProperties> getRuleList() {
		// TODO Auto-generated method stub
		return articleDAO.getRuleList();		
	}
	@Override
	public List<TariffDetail> getTariffById(String tariffType) {
		// TODO Auto-generated method stub
		return  articleDAO.getTariffById(tariffType);
		//return tariffDetail;
	}
	@Override
	public List<CabSearchList> getCabByType(String Model) {
		// TODO Auto-generated method stub
		return articleDAO.getCabBytype(Model);
	}
	
	@Override
	public List<CabSearchList> getCabByDist(String Model,double picLat,double picLng) {
		// TODO Auto-generated method stub
		return articleDAO.getCabByDist(Model,picLat,picLng);
	}
	@Override
	public List<Location> getLocByBranch() {
		// TODO Auto-generated method stub
		return articleDAO.getLocByBranch();
	}
	@Override
	public CustomerDetail getCustById(int customerId) {
		// TODO Auto-generated method stub
		CustomerDetail obj = articleDAO.getCustById(customerId);
		return obj;
	}
	@Override
	public TariffDetail tariffById(String tariffType) {
		// TODO Auto-generated method stub
		TariffDetail tariffDetail = articleDAO.tariffById(tariffType);		
		return tariffDetail;
	}
	@Override
	public boolean createBooking(Booking booking) {
		// TODO Auto-generated method stub
		articleDAO.createBooking(booking);
		return true;
	}
	@Override
	public Booking getBookingById(int bookingId) {
		// TODO Auto-generated method stub
		Booking booking = articleDAO.getBookingById(bookingId);
		return booking;
	}
	@Override
	public RulesProperties getRuleByName(String ruleName) {
		// TODO Auto-generated method stub
		RulesProperties rulesProperties =articleDAO.getRuleByName(ruleName);
		return rulesProperties;
	}
	@Override
	public TariffDetail tariffById(int tariffId) {
		// TODO Auto-generated method stub
		TariffDetail tariffDetail = articleDAO.tariffById(tariffId);
		return tariffDetail;
	}
	@Override
	public void updateBooking(Booking booking) {
		// TODO Auto-generated method stub
		articleDAO.updateBooking(booking);		
	}
}
