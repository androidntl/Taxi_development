package com.concretepage.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import com.concretepage.entity.CabSearchList;

public class CabSearchResponse {
	
	private int cab_id;	
	private String cab_model;	
	private int cab_type;	
	private String cab_no;
	private String cab_rating;	
	private int cab_status;	
	private String booking_id;	
	private double cab_lat;	
	private double cab_lng;	
	public double distance;	
	public Date created_date;
	public int tariffAmt;
	
	public CabSearchResponse(CabSearchList cabSearchList,int tariffAmt,double travelDistance) {
		this.cab_id=cabSearchList.getCab_id();
		this.cab_model=cabSearchList.getCab_model();
		this.cab_type=cabSearchList.getCab_type();		
		this.cab_lat=cabSearchList.getCab_lat();
		this.cab_lng=cabSearchList.getCab_lng();
		this.distance =travelDistance;
		this.tariffAmt=tariffAmt;
	}
	
	public int getTariffAmt() {
		return tariffAmt;
	}

	public void setTariffAmt(int tariffAmt) {
		this.tariffAmt = tariffAmt;
	}

	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
		
	public double getCab_lat() {
		return cab_lat;
	}
	public void setCab_lat(double cab_lat) {
		this.cab_lat = cab_lat;
	}
	public double getCab_lng() {
		return cab_lng;
	}
	public void setCab_lng(double cab_lng) {
		this.cab_lng = cab_lng;
	}
	public int getCab_id() {
		return cab_id;
	}
	public void setCab_id(int cab_id) {
		this.cab_id = cab_id;
	}
	public String getCab_model() {
		return cab_model;
	}
	public void setCab_model(String cab_model) {
		this.cab_model = cab_model;
	}
	public int getCab_type() {
		return cab_type;
	}
	public void setCab_type(int cab_type) {
		this.cab_type = cab_type;
	}
	public String getCab_no() {
		return cab_no;
	}
	public void setCab_no(String cab_no) {
		this.cab_no = cab_no;
	}
	public String getCab_rating() {
		return cab_rating;
	}
	public void setCab_rating(String cab_rating) {
		this.cab_rating = cab_rating;
	}
	public int getCab_status() {
		return cab_status;
	}
	public void setCab_status(int cab_status) {
		this.cab_status = cab_status;
	}
	public String getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(String booking_id) {
		this.booking_id = booking_id;
	}


}
