package com.concretepage.domain;

public class Tariff {
 String strCabType,strAmount;
 
 public Tariff() {}
 
 public Tariff(String cabType,String cabTariff) {
	 this.strCabType=cabType;
	 this.strAmount=cabTariff;
 }

public String getStrCabType() {
	return strCabType;
}

public void setStrCabType(String strCabType) {
	this.strCabType = strCabType;
}

public String getStrAmount() {
	return strAmount;
}

public void setStrAmount(String strAmount) {
	this.strAmount = strAmount;
}
 
}
