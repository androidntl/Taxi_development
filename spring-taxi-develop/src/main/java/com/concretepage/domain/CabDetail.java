package com.concretepage.domain;

public class CabDetail {
	String strCabId,strCabModel,strCabPosition,strLastTripCloseTime,strCabType;
	int intAllotedTrip,intPerformedTrip,intSeatAvailable;
	double fltTotalTripAmount,fltDistance,fltRating;
		
	public CabDetail() {};
	public CabDetail(int seatAvailable,int allotedTrip,int performedTrip,
			            double fltTotalTripAmount,  String strCabId, String strCabModel, String strCabPosition, 
			            String strLastTripCloseTime,String cabType,String cabPosition,double fltDistance,double fltRating ) {  
	    this.strCabId = strCabId;  
	    this.strCabModel = strCabModel;  
	    this.strCabPosition = strCabPosition;  
	    this.strLastTripCloseTime = strLastTripCloseTime;  
	    this.intAllotedTrip = allotedTrip;  
	    this.intPerformedTrip = performedTrip;  
	    this.intSeatAvailable = seatAvailable;  
	    this.fltTotalTripAmount = fltTotalTripAmount;  
	    this.strCabType = cabType;  	   
	    this.fltDistance = fltDistance; 
	    this.fltRating = fltRating;
	}
	
	public CabDetail(int seatAvailable) {
		this.intSeatAvailable = seatAvailable;  	
    }


	public String getStrCabType() {
		return strCabType;
	}
	public void setStrCabType(String strCabType) {
		this.strCabType = strCabType;
	}
	public String getStrCabId() {
		return strCabId;
	}

	public void setStrCabId(String strCabId) {
		this.strCabId = strCabId;
	}

	public String getStrCabModel() {
		return strCabModel;
	}

	public void setStrCabModel(String strCabModel) {
		this.strCabModel = strCabModel;
	}

	public String getStrCabPosition() {
		return strCabPosition;
	}

	public void setStrCabPosition(String strCabPosition) {
		this.strCabPosition = strCabPosition;
	}

	public String getStrLastTripCloseTime() {
		return strLastTripCloseTime;
	}

	public void setStrLastTripCloseTime(String strLastTripCloseTime) {
		this.strLastTripCloseTime = strLastTripCloseTime;
	}

	public int getIntAllotedTrip() {
		return intAllotedTrip;
	}

	public void setIntAllotedTrip(int intAllotedTrip) {
		this.intAllotedTrip = intAllotedTrip;
	}

	public int getIntPerformedTrip() {
		return intPerformedTrip;
	}

	public void setIntPerformedTrip(int intPerformedTrip) {
		this.intPerformedTrip = intPerformedTrip;
	}

	public int getIntSeatAvailable() {
		return intSeatAvailable;
	}

	public void setIntSeatAvailable(int intSeatAvailable) {
		this.intSeatAvailable = intSeatAvailable;
	}

	public double getFltTotalTripAmount() {
		return fltTotalTripAmount;
	}

	public void setFltTotalTripAmount(double fltTotalTripAmount) {
		this.fltTotalTripAmount = fltTotalTripAmount;
	}  

	public double getFltDistance() {
		return fltDistance;
	}
	public void setFltDistance(double fltDistance) {
		this.fltDistance = fltDistance;
	}
	public double getFltRating() {
		return fltRating;
	}
	public void setFltRating(double fltRating) {
		this.fltRating = fltRating;
	}
	
}
