package com.concretepage.domain;

import java.io.Serializable;

public class CabSearch implements Serializable{
	String cabType;
	String pickPosition;
	String dropPosition;
	
	public CabSearch(String cabtype){
		this.cabType=cabtype;		
	}
	
	public String getCabType() {
		return cabType;
	}
	public void setCabType(String cabType) {
		this.cabType = cabType;
	}
	public String getPickPosition() {
		return pickPosition;
	}
	public void setPickPosition(String pickPosition) {
		this.pickPosition = pickPosition;
	}
	public String getDropPosition() {
		return dropPosition;
	}
	public void setDropPosition(String dropPosition) {
		this.dropPosition = dropPosition;
	}

}
