package com.concretepage.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author lichangchao
 * @version 1.0.0
 * @date 2017/8/16 9:33
 * @see
 */
@Data
public class Message {
    private int status;
    private String message;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
