package com.concretepage.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.json.JSONArray;
import org.springframework.stereotype.Repository;

import com.concretepage.model.RouteStopsResponse;
import com.concretepage.model.ShuttleStops;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Transactional
@Repository
public class ShuttleRouteStopsDao {
	
	@PersistenceContext
	private EntityManager em;
	
	private static ObjectMapper mapper = new ObjectMapper(); 

	public List<ShuttleStops>getAll() {
		    return em.createQuery("SELECT p FROM ShuttleStops p", ShuttleStops.class).getResultList();
	}
	
	public String getRouteStops(Integer stop_id)
	{
		Integer route=0;
		List myList=new ArrayList();
		List ll=new ArrayList();
		
		List i=em.createQuery("SELECT r.route_ID FROM Booking r WHERE r.stop_ID="+stop_id+" ").getResultList();		
		for (Object temp : i) {			
			route=(Integer) temp;
	    }		
		
		Query q= em.createQuery("SELECT p.id as id, p.stops_Name as stopname,n.route_ID as routeid FROM ShuttleStops p,ShuttleRouteStops n"
		        + " where p.id = n.stop_ID and n.route_ID="+route+" and n.stop_ID > "+stop_id+"  order by p.id"
		       );
		 myList = q.getResultList(); 
			
		 List<RouteStopsResponse> personList = new ArrayList<RouteStopsResponse>();
		 for(int j = 0; j < myList.size(); j++){			
				
			 //myList[j];
			// RouteStopsResponse person = new RouteStopsResponse(myList.get(j));
			// person.setId(myList.get(j));
			// myList.get(j);
				RouteStopsResponse person = new RouteStopsResponse(1,1,"dxvzv");	
				
			   personList.add(person); // adding each person object to the list.
			}       
        String json = new Gson().toJson(personList);       
		//return json;*/
		
		return json;
		
	}
	
	
	
}
