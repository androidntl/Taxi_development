package com.concretepage.dao;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.concretepage.entity.Article;
import com.concretepage.entity.Booking;
import com.concretepage.entity.CabSearchList;
import com.concretepage.entity.CustomerDetail;
import com.concretepage.entity.Greeting;
import com.concretepage.entity.Location;
import com.concretepage.entity.Login_Validation;
import com.concretepage.entity.RulesProperties;
import com.concretepage.entity.TariffDetail;
import com.concretepage.repository.RuleRepository;

@Transactional
@Repository
public class ArticleDAO implements IArticleDAO {	
	 
	@PersistenceContext	
	private EntityManager entityManager;	
	@Override
	public Article getArticleById(int articleId) {
		return entityManager.find(Article.class, articleId);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Article> getAllArticles() {
		String hql = "FROM Article as atcl ORDER BY atcl.articleId DESC";
		return (List<Article>) entityManager.createQuery(hql).getResultList();
	}	
	@Override
	public void createArticle(Article article) {
		entityManager.persist(article);
	}
	@Override
	public void updateArticle(Article article) {
		Article artcl = getArticleById(article.getArticleId());
		artcl.setTitle(article.getTitle());
		artcl.setCategory(article.getCategory());
		entityManager.flush();
	}
	@Override
	public void deleteArticle(int articleId) {
		entityManager.remove(getArticleById(articleId));
	}
	@Override
	public boolean articleExists(String title, String category) {
		String hql = "FROM Article as atcl WHERE atcl.title = ? and atcl.category = ?";
		int count = entityManager.createQuery(hql).setParameter(1, title)
		              .setParameter(2, category).getResultList().size();
		return count > 0 ? true : false;
		
	}
	@Override
	public void login(Greeting greeting) {
		// TODO Auto-generated method stub
		System.out.println("Dao called"+greeting.getEmail());
		System.out.println("Dao Called"+greeting.getPassword());
		entityManager.persist(greeting);
		//String hql = "FROM Article as atcl ORDER BY atcl.articleId DESC";
		//return (List<Article>) entityManager.createQuery(hql).getResultList();		
	}
	@Override
	public List<Greeting> getAllUsers() {
		// TODO Auto-generated method stub
		String hql = "FROM Greeting as L ORDER BY L.login_id DESC";
		return (List<Greeting>) entityManager.createQuery(hql).getResultList();
	}
	@Override
	public boolean Login_Validation(Login_Validation logv) {
		// TODO Auto-generated method stub
		System.out.println("Dao called Email "+logv.getEmail());
		System.out.println("Dao Called Pass "+logv.getPassword());
		return true;
	}
	@Override
	public void createRule(RulesProperties rulesProperties) {
		// TODO Auto-generated method stub
		entityManager.persist(rulesProperties);
	}
	
	@Override
	public boolean ruleExist(String name) {
		// TODO Auto-generated method stub
		String hql = "FROM RulesProperties as rul WHERE rul.properties_name = ?";
		int count =entityManager.createQuery(hql).setParameter(1, name)
		              .getResultList().size();
		return count > 0 ? true : false;
		//return false;
	}
	@Override
	public List<CabSearchList> getCabList() {
		// TODO Auto-generated method stub
		String hql = "FROM CabSearchList as CL ORDER BY CL.distance,CL.cab_rating DESC";
		return (List<CabSearchList>) entityManager.createQuery(hql).getResultList();
		//return null;
	}
	@Override
	public void cabAllot(CabSearchList cabSearchList) {
		// TODO Auto-generated method stub
		CabSearchList csl = getCabById(cabSearchList.getCab_id());
		csl.setCab_status(2);	
		System.out.println("CabAllot:  "+cabSearchList.getCab_id());
		entityManager.flush();		
	}
	
	@Override
	public CabSearchList getCabById(int cabId) {
		// TODO Auto-generated method stub
		System.out.println("CabSearch:  "+cabId);
		return entityManager.find(CabSearchList.class, cabId);
		//return null;
	}
	@Override
	public List<RulesProperties> getRuleList() {
		// TODO Auto-generated method stub
		String hql = "FROM RulesProperties as RP ORDER BY RP.properties_id";
		return (List<RulesProperties>) entityManager.createQuery(hql).getResultList();		
	}
	@Override
	public List<TariffDetail> getTariffById(String tariffType) {
		// TODO Auto-generated method stub
		String hql="FROM TariffDetail as TD WHERE TD.tariff_type=?";
		return	(List<TariffDetail>) entityManager.createQuery(hql).setParameter(1, tariffType).getResultList();
		//return null;
	}
	@Override
	public List<CabSearchList> getCabBytype(String model) {
		// TODO Auto-generated method stub		
		String hql = "FROM CabSearchList as CL WHERE CL.cab_model=? ORDER BY CL.distance,CL.cab_rating DESC";
		return (List<CabSearchList>) entityManager.createQuery(hql).setParameter(1, model).getResultList();
		//return null;
	}
	
	@Override
	public List<CabSearchList> getCabByDist(String model,double picLat,double picLng) {
		// TODO Auto-generated method stub		
		//String hql = "FROM CabSearchList as CL WHERE CL.cab_model=? ORDER BY CL.distance,CL.cab_rating DESC";
		String hql = "FROM CabSearchList as CL  WHERE 111.195*sqrt(power(?-CL.cab_lat,2)+power(cos(pi()/180*CL.cab_lat)*(?-CL.cab_lng),2)) < 20 AND "
				      + "CL.cab_model=? ORDER BY CL.distance,CL.cab_rating DESC";
		return (List<CabSearchList>) entityManager.createQuery(hql).setParameter(1, picLat).setParameter(2, picLng).setParameter(3, model).getResultList();
		//return null;
	}
	@Override
	public List<Location> getLocByBranch() {
		// TODO Auto-generated method stub
		String hql = "FROM Location as LOC WHERE LOC.branch_code=0 ";
		return (List<Location>) entityManager.createQuery(hql).getResultList();		
	}
	@Override
	public CustomerDetail getCustById(int customerId) {
		// TODO Auto-generated method stub
		return entityManager.find(CustomerDetail.class, customerId);
	}
	@Override
	public TariffDetail tariffById(String tariff_type) {
		// TODO Auto-generated method stub		
		String hql = "FROM TariffDetail as td  where td.tariff_type=?  ORDER BY td.tariff_type";
		return (TariffDetail) entityManager.createQuery(hql).setParameter(1, tariff_type).getSingleResult();		
	}
	@Override
	public void createBooking(Booking booking) {
		// TODO Auto-generated method stub
		entityManager.persist(booking);		
	}
	@Override
	public Booking getBookingById(int bookingId) {
		// TODO Auto-generated method stub
		return entityManager.find(Booking.class, bookingId);		
	}
	@Override
	public RulesProperties getRuleByName(String ruleName) {
		// TODO Auto-generated method stub
		String tariff="tariff";
		String hql = "FROM RulesProperties as RP where RP.properties_name=? AND RP.propertie_category = ? ORDER BY RP.properties_id";
		return (RulesProperties) entityManager.createQuery(hql).setParameter(1, ruleName).setParameter(2, tariff).getSingleResult();
	}
	@Override
	public TariffDetail tariffById(int tariffId) {
		// TODO Auto-generated method stub
		return entityManager.find(TariffDetail.class, tariffId);		
	}
	@Override
	public void updateBooking(Booking booking) {
		// TODO Auto-generated method stub
		Booking book = getBookingById(booking.getBooking_id());
		book.setCorp_amount(booking.getCorp_amount());
		book.setPaid_amount(booking.getPaid_amount());
		book.setDriver_amount(booking.getDriver_amount());
		book.setTotal_amount(booking.getTotal_amount());
		book.setTotal_km(booking.getTotal_km());		
		entityManager.flush();
		
	}
}
