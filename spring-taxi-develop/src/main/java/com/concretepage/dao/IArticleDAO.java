package com.concretepage.dao;
import java.util.List;
import com.concretepage.entity.Article;
import com.concretepage.entity.Booking;
import com.concretepage.entity.Greeting;
import com.concretepage.entity.Location;
import com.concretepage.entity.CabSearchList;
import com.concretepage.entity.CustomerDetail;
import com.concretepage.entity.Login_Validation;
import com.concretepage.entity.RulesProperties;
import com.concretepage.entity.TariffDetail;
public interface IArticleDAO {
    List<Article> getAllArticles();
    Article getArticleById(int articleId);
    void createArticle(Article article);
    void updateArticle(Article article);
    void deleteArticle(int articleId);
    boolean articleExists(String title, String category);
   void login(Greeting greeting);
   List<Greeting> getAllUsers();
   boolean Login_Validation(Login_Validation logv);
   void createRule(RulesProperties rulesProperties);
   boolean ruleExist(String name);
   List<CabSearchList> getCabList();
   List<RulesProperties> getRuleList();
   RulesProperties getRuleByName(String ruleName);
   CabSearchList getCabById(int cabId);
   void cabAllot(CabSearchList cabSearchList);
   List<TariffDetail> getTariffById(String tariffType);
   TariffDetail tariffById(int tariffId);
   TariffDetail tariffById(String tariffType);
   List<CabSearchList> getCabBytype(String Model);
   List<CabSearchList> getCabByDist(String Model,double picLat,double picLng);
   List<Location> getLocByBranch();
   CustomerDetail getCustById(int customerId);
   void createBooking(Booking booking);
   void updateBooking(Booking booking);
   Booking getBookingById(int bookingId);
}
 