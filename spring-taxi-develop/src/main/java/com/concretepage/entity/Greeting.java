package com.concretepage.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="login")
public class Greeting {
	/*public Greeting(String email, String password) {
	super();
	this.email = email;
	this.password = password;
}*/
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="LOGIN_ID")
    private int login_id;  
	
	@Column(name="EMAIL_ID")
    private String email;
	
	@Column(name="LOGIN_PASSWORD")
	private String password;
	
	@Column(name="ACTIVE")
	private final int active=1 ;
	
    public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
