package com.concretepage.entity;

import java.util.List;

public class CabBooking {
	List<CabSearchList> cabList;
	String cabModel;
	String bookingNo;
	public List<CabSearchList> getCabList() {
		return cabList;
	}
	public void setCabList(List<CabSearchList> cabList) {
		this.cabList = cabList;
	}
	public String getCabModel() {
		return cabModel;
	}
	public void setCabModel(String cabModel) {
		this.cabModel = cabModel;
	}
	public String getBookingNo() {
		return bookingNo;
	}
	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}
	
}
