package com.concretepage.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="cust_tariff")
public class CustomerTariff implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CUST_TARIFF_ID")
	private int cust_tariff_id;
	@Column(name = "CUST_TARIFF_NAME")
	private String cuat_tariff_name;
	@Column(name = "CUST_TARIFF_PER_KM")
	private int cust_tariff_per_km;
	
	public int getCust_tariff_id() {
		return cust_tariff_id;
	}
	public void setCust_tariff_id(int cust_tariff_id) {
		this.cust_tariff_id = cust_tariff_id;
	}
	public String getCuat_tariff_name() {
		return cuat_tariff_name;
	}
	public void setCuat_tariff_name(String cuat_tariff_name) {
		this.cuat_tariff_name = cuat_tariff_name;
	}
	public int getCust_tariff_per_km() {
		return cust_tariff_per_km;
	}
	public void setCust_tariff_per_km(int cust_tariff_per_km) {
		this.cust_tariff_per_km = cust_tariff_per_km;
	}

}
