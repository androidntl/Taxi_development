package com.concretepage.entity;

import java.util.List;

public class CabBookingRule {
	List<RulesProperties> ruleList;
	String bookingType;
	public List<RulesProperties> getRuleList() {
		return ruleList;
	}
	public void setRuleList(List<RulesProperties> ruleList) {
		this.ruleList = ruleList;
	}
	public String getBookingType() {
		return bookingType;
	}
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
}
