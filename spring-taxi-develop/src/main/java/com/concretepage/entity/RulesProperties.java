package com.concretepage.entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="rules")
public class RulesProperties implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "RULE_ID")
	private int properties_id;
	@Column(name = "BRANCH_ID")
	private String branch_id;
	@Column(name = "RULE_NAME")
	private String properties_name;
	@Column(name = "RULE_VALUE")
	private String properties_value;
	@Column(name = "RULE_CATEGORY")
	private String propertie_category;
	public int getProperties_id() {
		return properties_id;
	}
	public void setProperties_id(int properties_id) {
		this.properties_id = properties_id;
	}
	public String getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}
	public String getProperties_name() {
		return properties_name;
	}
	public void setProperties_name(String properties_name) {
		this.properties_name = properties_name;
	}
	public String getProperties_value() {
		return properties_value;
	}
	public void setProperties_value(String properties_value) {
		this.properties_value = properties_value;
	}
	public String getPropertie_category() {
		return propertie_category;
	}
	public void setPropertie_category(String propertie_category) {
		this.propertie_category = propertie_category;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}