package com.concretepage.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="cab_detail")
public class CabSearchList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CAB_ID")
	private int cab_id;
	@Column(name = "CAB_MODEL")
	private String cab_model;
	@Column(name = "CAB_TYPE")
	private int cab_type;
	@Column(name = "CAB_NO")
	private String cab_no;
	@Column(name = "CAB_RATING")
	private String cab_rating;
	@Column(name = "CAB_STATUS")
	private int cab_status;
	@Column(name = "BOOKING_ID")
	private String booking_id;
	@Column(name = "CAB_LAT")
	private double cab_lat;
	@Column(name = "CAB_LNG")
	private double cab_lng;
	@Column(name = "CAB_DISTANCE")
	public double distance;
	@Column(name = "CREATED_DATE")
	public Date created_date;
	
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
		
	public double getCab_lat() {
		return cab_lat;
	}
	public void setCab_lat(double cab_lat) {
		this.cab_lat = cab_lat;
	}
	public double getCab_lng() {
		return cab_lng;
	}
	public void setCab_lng(double cab_lng) {
		this.cab_lng = cab_lng;
	}
	public int getCab_id() {
		return cab_id;
	}
	public void setCab_id(int cab_id) {
		this.cab_id = cab_id;
	}
	public String getCab_model() {
		return cab_model;
	}
	public void setCab_model(String cab_model) {
		this.cab_model = cab_model;
	}
	public int getCab_type() {
		return cab_type;
	}
	public void setCab_type(int cab_type) {
		this.cab_type = cab_type;
	}
	public String getCab_no() {
		return cab_no;
	}
	public void setCab_no(String cab_no) {
		this.cab_no = cab_no;
	}
	public String getCab_rating() {
		return cab_rating;
	}
	public void setCab_rating(String cab_rating) {
		this.cab_rating = cab_rating;
	}
	public int getCab_status() {
		return cab_status;
	}
	public void setCab_status(int cab_status) {
		this.cab_status = cab_status;
	}
	public String getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(String booking_id) {
		this.booking_id = booking_id;
	}

}
