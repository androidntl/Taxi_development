package com.concretepage.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="booking")
public class Booking implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "BOOKING_ID")
	private int booking_id;
	@Column(name = "CUSTOMER_ID")
	private int customer_id;
	@Column(name = "CAB_ID")
	private int cab_id;	
	@Column(name = "TARIFF_ID")
	private int tariff_id;		
	@Column(name = "TOTAL_AMOUNT")
	private int total_amount;
	@Column(name = "PAID_AMOUNT")
	private int paid_amount;
	@Column(name = "DRIVER_AMOUNT")
	private int driver_amount;
	@Column(name = "CORP_AMOUNT")
	private int corp_amount;
	@Column(name = "TOTAL_KM")
	private int total_km;
	public Booking() {		
	}
	
	public int getTariff_id() {
		return tariff_id;
	}
	public void setTariff_id(int tariff_id) {
		this.tariff_id = tariff_id;
	}
	public Booking(int customer_id , int cab_id) {
		this.customer_id = customer_id;
		this.cab_id = cab_id;		
	}
	
	public Booking(int booking_id , int total_km, int total_amount,int paid_amount,int driver_amount ,int corp_amount ) {
		this.booking_id = booking_id;
		this.total_km = total_km;	
		this.total_amount = total_amount;
		this.paid_amount = paid_amount;
		this.driver_amount = driver_amount;
		this.corp_amount = corp_amount;
	}
	
	public int getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(int booking_id) {
		this.booking_id = booking_id;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public int getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(int total_amount) {
		this.total_amount = total_amount;
	}
	public int getTotal_km() {
		return total_km;
	}
	public void setTotal_km(int total_km) {
		this.total_km = total_km;
	}	
	public int getCab_id() {
		return cab_id;
	}
	public void setCab_id(int cab_id) {
		this.cab_id = cab_id;
	}
	public int getPaid_amount() {
		return paid_amount;
	}
	public void setPaid_amount(int paid_amount) {
		this.paid_amount = paid_amount;
	}
	public int getDriver_amount() {
		return driver_amount;
	}
	public void setDriver_amount(int driver_amount) {
		this.driver_amount = driver_amount;
	}
	public int getCorp_amount() {
		return corp_amount;
	}
	public void setCorp_amount(int corp_amount) {
		this.corp_amount = corp_amount;
	}

}
