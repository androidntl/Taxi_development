package com.concretepage.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="adm_taxi_location")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "AREA_CODE")
	private int area_code;
	@Column(name = "AREA_NAME")
	private String area_name;
	@Column(name = "AREA_LATITUDE")
	private double area_latitude;
	@Column(name = "AREA_LONGITUDE")
	private double area_longitude;
	@Column(name = "BRANCH_CODE")
	private int branch_code;
	public int getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(int branch_code) {
		this.branch_code = branch_code;
	}
	public int getArea_code() {
		return area_code;
	}
	public void setArea_code(int area_code) {
		this.area_code = area_code;
	}
	public String getArea_name() {
		return area_name;
	}
	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}
	public double getArea_latitude() {
		return area_latitude;
	}
	public void setArea_latitude(double area_latitude) {
		this.area_latitude = area_latitude;
	}
	public double getArea_longitude() {
		return area_longitude;
	}
	public void setArea_longitude(double area_longitude) {
		this.area_longitude = area_longitude;
	}

}
