package com.concretepage.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name="tariff")
public class TariffDetail implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TARIFF_ID")
	private int tariff_id;
	@Column(name = "TARIFF_TYPE")
	private String tariff_type;
	@Column(name = "TARIFF_PER_KM")
	private int tariff_per_km;
	public int getTariff_id() {
		return tariff_id;
	}
	public void setTariff_id(int tariff_id) {
		this.tariff_id = tariff_id;
	}
	public String getTariff_type() {
		return tariff_type;
	}
	public void setTariff_type(String tariff_type) {
		this.tariff_type = tariff_type;
	}
	public int getTariff_per_km() {
		return tariff_per_km;
	}
	public void setTariff_per_km(int tariff_per_km) {
		this.tariff_per_km = tariff_per_km;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
