package com.concretepage.model;

public class RouteStopsResponse{

	int id;
	int route_ID;
	String stops_Name;
	
	public RouteStopsResponse()
	{
		
	}
	public RouteStopsResponse(int id,int routeId,String stopName)//ShuttleRoutes routes,ShuttleStops stops_Name
	{
		this.id=id;
		this.route_ID=routeId;
		this.stops_Name=stopName;
				
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRoute_ID() {
		return route_ID;
	}
	public void setRoute_ID(int route_ID) {
		this.route_ID = route_ID;
	}
	public String getStops_Name() {
		return stops_Name;
	}
	public void setStops_Name(String stops_Name) {
		this.stops_Name = stops_Name;
	}
	
	public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("************************************");
        sb.append("\nempId: ").append(id);
        sb.append("\nname: ").append(route_ID);
        sb.append("\ndesignation: ").append(stops_Name);       
        sb.append("\n************************************");
        return sb.toString();
    }
	
}
